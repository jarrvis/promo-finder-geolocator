import _bootstrap_

from classes.Scanner import *
from classes.services.GoogleRadarSearch import *

#
# In this example we will use the google radar search service
# to look for all groceries or supermarkets with the name "ICA".
# 
#
# For the full options for the Radar Search you can check the
# "Optional paremeters" here:
# https://developers.google.com/places/documentation/search#RadarSearchRequests
#

scanner = Scanner()
searchitems={"name": "Biedronka", "type": "convenience_store"};
#key="AIzaSyCJP1b6wcfWFhWrwCplr9Jd9uGoBr8-0b8" # Insert your Google key here
keys=["AIzaSyD631nanNJOrBWeu83JtynDW5X5BTUM1Kc", "AIzaSyCJP1b6wcfWFhWrwCplr9Jd9uGoBr8-0b8"]
service = GoogleRadarSearch(searchitems, keys)
scanner.set_service(service)
scanner.start_scanning()
