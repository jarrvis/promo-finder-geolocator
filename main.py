import scan_types.google_text_search as search
import os
import shutil
from flask import Flask
from flask import request


app = Flask(__name__)

@app.route('/')
def main():
    return 'Welcome to importer'

@app.route('/fetch')
def fetch():
    name = request.args.get('name')
    type = request.args.get('type')
    search.fetch_locations(name, type)
    copy_rename("results.log", name)
    return 'Fetching...'


def copy_rename(old_file_name, new_file_name):
    src_dir = os.path.join(os.curdir, "log")
    dst_dir = os.path.join(os.curdir, "FEED")
    src_file = os.path.join(src_dir, old_file_name)
    shutil.copy(src_file, dst_dir)

    dst_file = os.path.join(dst_dir, old_file_name)
    new_dst_file_name = os.path.join(dst_dir, new_file_name + ".log")
    os.rename(dst_file, new_dst_file_name)


if __name__ == '__main__':
    app.run()